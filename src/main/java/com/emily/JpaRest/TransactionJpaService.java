package com.emily.JpaRest;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TransactionJpaService
{
	private EntityManagerFactory emf = Persistence
            .createEntityManagerFactory ("pu");
	private EntityManager em = emf.createEntityManager ();

    public void addTransaction (TransactionTest t)
    {

        /*EntityManagerFactory emf = Persistence
            .createEntityManagerFactory ("pu");
        EntityManager em = emf.createEntityManager ();*/

        em.getTransaction ().begin ();
        em.persist (t);
        em.getTransaction ().commit ();

    }

    public List<TransactionTest> getTransactionHistory ()
    {

        /*EntityManagerFactory emf = Persistence
            .createEntityManagerFactory ("pu");
        EntityManager em = emf.createEntityManager ();*/

        return em.createNamedQuery ("TransactionTest.findAll").getResultList ();
    }
}