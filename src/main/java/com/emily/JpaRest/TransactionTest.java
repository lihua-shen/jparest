package com.emily.JpaRest;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "TransactionTest")
@NamedQueries(
    { @NamedQuery(name = "TransactionTest.getMaxID", query = "Select max(t.id) as maxid from TransactionTest t"),
        @NamedQuery(name = "TransactionTest.findAll", query = "SELECT t FROM TransactionTest t ORDER BY Time") })
public class TransactionTest
{
    @Id
    @Column(name = "Id") 
    private Integer id;
    @Column(name = "Time")
    private Timestamp time = new Timestamp (System.currentTimeMillis ());
    @Column(name = "Price")
    private double price;
    @Column(name = "Size")
    private int size;
    @Column(name = "Ticker")
    private String ticker;
    @Column(name = "Action")
    private String action;

    public TransactionTest ()
    {

    }

    public TransactionTest (/*Integer id, Timestamp time */ double price, int size,
        String ticker, String action)
    {
        super ();
        //this.id = id;
        //this.time = time;
        this.price = price;
        this.size = size;
        this.ticker = ticker;
        this.action = action;
    }

    public Integer getId ()
    {
        return id;
    }

    public void setId (Integer id)
    {
        this.id = id;
    }

    public Timestamp getTime ()
    {
        return time;
    }

    public void setTime (Timestamp time)
    {
        this.time = time;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public int getSize ()
    {
        return size;
    }

    public void setSize (int size)
    {
        this.size = size;
    }

    public String getTicker ()
    {
        return ticker;
    }

    public void setTicker (String ticker)
    {
        this.ticker = ticker;
    }

    public String getAction ()
    {
        return action;
    }

    public void setAction (String action)
    {
        this.action = action;
    }

}
