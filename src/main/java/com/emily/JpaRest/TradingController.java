package com.emily.JpaRest;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TradingController {

	@RequestMapping("/transactions")
	public List<TransactionTest> returnAllTransactions() {
		TransactionJpaService tjs = new TransactionJpaService();
		TransactionTest t1 = new TransactionTest(2.5, 100, "Citi", "Buy");
		tjs.addTransaction(t1);
		TransactionTest t2 = new TransactionTest(1.5, 200, "Google", "Buy");
		tjs.addTransaction(t2); 
		TransactionTest t3 = new TransactionTest(3.5, 300, "Apple", "Sell");
		tjs.addTransaction(t3);
		return tjs.getTransactionHistory();
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/postsomething")
	public void addTransaction(@RequestBody String something) {
		//public void addTransaction(@RequestBody Transaction transaction)
		//transactionService.addTransaction(transaction);
	}
}
